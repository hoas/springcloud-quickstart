package com.hoas.springcloud.config.client.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${from}")
    private String from;

    @Value("${to}")
    private String to;

    @Autowired
    private Environment env;

    @RequestMapping("/from")
    public String from(){
        return this.from;
    }

    @RequestMapping("/to")
    public String to(){
        return this.to;
    }

    @RequestMapping("/fromEnv")
    public String fromEnv(){
        return env.getProperty("from", "undefined");
    }

}
