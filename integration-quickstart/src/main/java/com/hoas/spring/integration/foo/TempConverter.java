package com.hoas.spring.integration.foo;

public interface TempConverter {

    float fahrenheitToCelcius(float fahren);

}