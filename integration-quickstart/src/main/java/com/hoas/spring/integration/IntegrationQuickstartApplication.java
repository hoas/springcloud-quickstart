package com.hoas.spring.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrationQuickstartApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrationQuickstartApplication.class, args);
	}
}
