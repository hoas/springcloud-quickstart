package com.hoas.springcloud.bus.server;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class BusServerQuickstartApplication {

	@Bean
	public Queue queueHello(){
		return new Queue("hello");
	}

	public static void main(String[] args) {
		SpringApplication.run(BusServerQuickstartApplication.class, args);
	}
}
