package com.hoas.springcloud.bus.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BusServerQuickstartApplication.class)
public class BusServerQuickstartTest {

    @Autowired
    private Sender sender;

    @Test
    public void hello(){
        for (int i =0;i<10000;i++) {

            sender.send();

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
