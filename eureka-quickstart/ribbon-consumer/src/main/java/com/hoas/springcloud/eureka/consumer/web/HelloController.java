package com.hoas.springcloud.eureka.consumer.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URL;

@RestController
public class HelloController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/getForEntity")
    public String getForEntity(){
        return restTemplate.getForEntity("http://HELLO-SERVICE/hello",String.class).getBody();
    }

    @RequestMapping("/getForObject")
    public String getForObject(){
        return restTemplate.getForObject("http://HELLO-SERVICE/hello",String.class);
    }

    @RequestMapping("/postForLocation")
    public String postForLocation(){
        URI uri =  restTemplate.postForLocation("http://HELLO-SERVICE/helloLocation",null);

        return uri.toString();

    }

}
