package com.hoas.springcloud.eureka.client.web;

import com.hoas.springcloud.eureka.client.model.User;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String index(){
        return "Hello World!";
    }

    @RequestMapping("/hello1")
    public String hello(@RequestParam String name) {
        return "Hello " + name;
    }

    @RequestMapping("/hello2")
    public User hello(@RequestHeader String name, @RequestHeader Integer age) {
        return new User(name,age);
    }

    @RequestMapping(value = "/hello3", method = RequestMethod.POST)
    public String hello(@RequestBody User user) {
        return "Hello " + user.getName() + ", " + user.getAge();
    }

    @RequestMapping("/helloLocation")
    public URI helloLocation() throws URISyntaxException {
        return new URI("http://g.hiphotos.baidu.com/image/pic/item/b58f8c5494eef01fcca8beccecfe9925bc317d7f.jpg");
    }

}
