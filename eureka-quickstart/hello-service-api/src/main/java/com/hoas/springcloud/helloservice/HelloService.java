package com.hoas.springcloud.helloservice;

import com.hoas.springcloud.helloservice.protos.User;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/refactor")
public interface HelloService {

    @RequestMapping("/hello4")
    String hello(@RequestParam("name") String name );

    @RequestMapping("/hello5")
    User hello(@RequestHeader("name") String name, @RequestHeader("age") Integer age );

    @RequestMapping(value = "/hello6", method = RequestMethod.POST)
    String hello(@RequestBody User user );

}
