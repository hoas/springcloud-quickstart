package com.hoas.springcloud.feign.api;

import com.hoas.springcloud.eureka.client.model.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("hello-service")
public interface HelloService {

    @RequestMapping("/hello")
    String hello();

    @RequestMapping("/hello1")
    String hello(@RequestParam("name") String name );

    @RequestMapping("/hello2")
    User hello(@RequestHeader("name") String name,@RequestHeader("age") Integer age );

    @RequestMapping(value = "/hello3", method = RequestMethod.POST)
    String hello(@RequestBody User user );


}
