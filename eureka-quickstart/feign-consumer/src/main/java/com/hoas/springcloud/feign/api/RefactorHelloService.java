package com.hoas.springcloud.feign.api;

import com.hoas.springcloud.helloservice.HelloService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "HELLO-SERVICE")
public interface RefactorHelloService extends HelloService {
}
