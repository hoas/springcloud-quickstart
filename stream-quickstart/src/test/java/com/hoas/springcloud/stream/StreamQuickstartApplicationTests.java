package com.hoas.springcloud.stream;

import com.hoas.springcloud.stream.messaging.MySource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableBinding(value = {StreamQuickstartApplicationTests.SinkSender.class, MySource.class})
public class StreamQuickstartApplicationTests {

	@Autowired
	private SinkSender sinkSender;

	@Autowired
	private MessageChannel input;

	@Autowired @Qualifier(MySource.OUTPUT_1)
	private MessageChannel output;

	@Test
	public void sinkSenderTester() {
		sinkSender.output().send(
			MessageBuilder.withPayload("produce a message ：http://blog.didispace.com").build()
		);

		boolean flag = output.send(
				MessageBuilder.withPayload("produce a message from Output-1").build()
		);
		System.out.println("send to Output-1 success?" + flag);

	}

	@Test
	public void contextLoads(){
		input.send(
				MessageBuilder.withPayload("From MessageChannel").build()
		);
	}

	public interface SinkSender {

		String OUTPUT = "input";

		@Output(SinkSender.OUTPUT)
		MessageChannel output();

	}

}
