package com.hoas.springcloud.stream;

import com.hoas.springcloud.stream.messaging.MySink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding({Sink.class, MySink.class})
public class SinkReceiver {

    private static Logger logger = LoggerFactory.getLogger(SinkReceiver.class);

    @StreamListener( Sink.INPUT )
    public void receive(Object payload) {
        logger.info("Received : " + payload.getClass());
        logger.info("Received : " + payload);
    }

    @StreamListener( MySink.INPUT_1 )
    public void receiveOutput1(Object payload) {
        logger.info("Received from Output-1 : " + payload.getClass());
        logger.info("Received from Output-1 : " + payload);
    }


}
