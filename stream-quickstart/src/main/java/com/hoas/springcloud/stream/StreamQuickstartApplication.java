package com.hoas.springcloud.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamQuickstartApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamQuickstartApplication.class, args);
	}
}
