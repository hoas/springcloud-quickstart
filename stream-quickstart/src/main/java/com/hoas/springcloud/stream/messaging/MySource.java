package com.hoas.springcloud.stream.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MySource {

    String OUTPUT_1 = "Output-1";
    String OUTPUT_2 = "Output-2";

    @Output( OUTPUT_1 )
    MessageChannel output1();

    @Output( OUTPUT_2 )
    MessageChannel output2();



}
