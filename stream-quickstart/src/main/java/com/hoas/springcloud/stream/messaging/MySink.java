package com.hoas.springcloud.stream.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MySink {

    String INPUT_1 = "Output-1";

    String INPUT_2 = "Output-2";

    @Input(INPUT_1)
    SubscribableChannel input1();

    @Input(INPUT_2)
    SubscribableChannel input2();

}
